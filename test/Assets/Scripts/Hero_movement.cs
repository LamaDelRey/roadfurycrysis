﻿using UnityEngine;
using System.Collections;


public class Hero_movement : MonoBehaviour {

	//public HeroShot heroshot;
//	public Transform spawn;

	public float speed;
	public float xMin, xMax, zMin, zMax;


	void FixedUpdate ()
	{
		Vector3 move = Vector3.zero;

		if (Input.GetKey (KeyCode.RightArrow))
			move = Vector3.left;
		else if (Input.GetKey (KeyCode.LeftArrow))
			move = Vector3.right;

		if (Input.GetKey (KeyCode.UpArrow)) 
			move = Vector3.back;
		else if (Input.GetKey (KeyCode.DownArrow))
			move = Vector3.forward;


		var debugging = this.gameObject.transform.position;
		debugging += move * speed * Time.deltaTime;

		if(debugging.x >= xMax && move.x != -1 )
			debugging.x = xMax;
		else if(debugging.x <= xMin && move.x != 1)
			debugging.x = xMin;
		
		if(debugging.z >= zMax && move.z != -1)
			debugging.z = zMax;
		else if(debugging.z <= zMin && move.z != 1)
			debugging.z = zMin;

		this.gameObject.transform.position = debugging;
	}



}
