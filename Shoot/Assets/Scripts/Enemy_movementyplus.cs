﻿using UnityEngine;
using System.Collections;

public class Enemy_movementyplus : MonoBehaviour {

	public float speed;
	// Update is called once per frame
	void Update () {

		transform.Translate (Vector3.up * speed * Time.deltaTime);
	}
}
